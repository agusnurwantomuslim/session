<?php
	session_start();
	if(empty($_SESSION['username'])){
		die('Anda belum login, silahkan login di <a href="'.str_replace('tabel.php','login.php',$_SERVER['PHP_SELF']).'">login.php</a>');
	}
	
	$connect = mysqli_connect('localhost','root','12345','belajar')OR die( mysqli_error($connect) );
	$ket['ket'] = 'Sukses';
	
	if(!empty($_POST['nav_id'])){
		$id_baru = mysqli_real_escape_string($connect,$_POST['nav_id']);
		$id_lama = mysqli_real_escape_string($connect,$_POST['id']);
		if( !empty($id_baru) AND !empty($id_lama) ){
			mysqli_query( $connect,"update user set `id`=$id_baru where `id`=$id_lama " )OR $ket['ket'] = mysqli_error($connect);
		}	$ket['id']=$id_baru;
		die( json_encode($ket) );
	}elseif(!empty($_POST['nav_nama'])){
		$nama = mysqli_real_escape_string($connect,$_POST['nav_nama']);
		$id_lama = mysqli_real_escape_string($connect,$_POST['id']);
		if( !empty($nama) AND !empty($id_lama) ){
			mysqli_query( $connect,"update user set `nama`='$nama' where `id`=$id_lama " )OR $ket['ket'] = mysqli_error($connect);
		}	$ket['nama']=$nama;
		die( json_encode($ket) );
	}elseif(!empty($_POST['nav_date'])){
		$date = mysqli_real_escape_string($connect,$_POST['nav_date']);
		$id_lama = mysqli_real_escape_string($connect,$_POST['id']);
		if( !empty($date) AND !empty($id_lama) ){
			mysqli_query( $connect,"update user set `createdat`='$date' where `id`=$id_lama " )OR $ket['ket'] = mysqli_error($connect);
		}	$ket['date']=$date;
		die( json_encode($ket) );
	}elseif(!empty($_POST['ket'])){
		$i = mysqli_real_escape_string($connect,$_POST['i']);
		$id = mysqli_real_escape_string($connect,$_POST['id']);
		$nama = mysqli_real_escape_string($connect,$_POST['nama']);
		$date = mysqli_real_escape_string($connect,$_POST['date']);
		if($_POST['ket']=='Edit data'){
			if( !empty($id) && !empty($nama) && !empty($date) ){
				mysqli_query( $connect,"update user set `id`=$id, `nama`='$nama', `createdat`='$date' where `id`=$id " )OR $ket['ket'] = mysqli_error($connect);
				$ket['jenis']= 'edit';
			}
		}else if($_POST['ket']=='Insert data'){
			if( !empty($nama) ){
				$date = date('Y-m-d H:i:s');
				$a = mysqli_fetch_array(mysqli_query($connect,"SELECT max(`id`) from `user`")) OR $ket['ket'] = mysqli_error($connect);
				$id = $a[0]+1;
				mysqli_query( $connect,"INSERT INTO `user` (`id`, `nama`, `createdat`) VALUES ('$id','$nama','$date')" )OR $ket['ket'] = mysqli_error($connect);
				$ket['jenis']= 'insert';
			}
		}
		$ket['id']		=	$id;
		$ket['nama']	=	$nama;
		$ket['date']	=	$date;
		!empty($i)? $ket['i']=$i : $ket['i']='';
		die( json_encode($ket) );
	}else if(!empty($_POST['nav_del_all'])){
		$ket['ket'] = 'Sukses';
		$dt_id = mysqli_real_escape_string($connect,$_POST['id']);
		$id = explode(',',$dt_id);
		for( $i=0,$size=count($id);$i<$size;$i++ ){
			mysqli_query( $connect,"DELETE FROM `user` where `id`='$id[$i]'" )OR $ket['ket'] = mysqli_error($connect);
			$ket[$i] = $id[$i];
		}
		$ket['jml'] = $size;
		die( json_encode($ket) );
	}else if(!empty($_POST['nav_del'])){
		$id = mysqli_real_escape_string($connect,$_POST['id']);
		$ket['ket'] = 'Delete Sukses';
		if( !empty($id) ){
			mysqli_query( $connect,"DELETE FROM `user` where `id`='$id'" )OR $ket['ket'] = mysqli_error($connect);
		}
		die( json_encode($ket) );
	}else if(!empty($_POST['nav_log_out'])){
		unset($_SESSION['username']);
	}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<meta name="description" content="intro - theme build with bootstrap">
	<meta name="author" content="Agus Nurwanto">
	<title>tabel</title>
	<link href="<?php echo str_replace('tabel.php','',$_SERVER['PHP_SELF']); ?>css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo str_replace('tabel.php','',$_SERVER['PHP_SELF']); ?>style.css" rel="stylesheet" type="text/css">
	<script src='<?php echo str_replace('tabel.php','',$_SERVER['PHP_SELF']); ?>js/jquery-2.0.3.min.js'></script>
	<script src='<?php echo str_replace('tabel.php','',$_SERVER['PHP_SELF']); ?>js/custom_tabel.js'></script>
</head>
<body>
	<div id='full'>
		<div id='overlay'>
			<form name='form[0]'>
				<div class='head'>
					<input type='text' id='ket' value='' />
					<a onclick="close_full();"><span class='glyphicon glyphicon-remove-circle'></span></a>
				</div>
				<ul class='data'>
					<li>
						<label for='id'>Id</label>
						<input type='text' name='id' id='id' value='' placeholder='Id User'/>
					</li>
					<li>
						<label for='nama'>Nama</label>
						<input type='text' name='nama' id='nama' value='' placeholder='User Name'/>
					</li>
					<li>
						<label for='date'>Tanggal</label>
						<input type='text' name='date' id='date' value='' placeholder='Tanggal Registrasi'/>
					</li>
					<li>
						<input type='hidden' name='i' id='i' value=''/>
						<input type='submit' name='submit' id='submit' value='Submit' onclick='return db();' />
					</li>
				</ul>
			</form>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<center>
				<div class="center">
					<?php echo "<h2>Selamat datang $_SESSION[username]</h2> <a class='pointer' onclick='log_out();'>Log Out..</a>"; ?>
					<form name='form[1]'>
						<table cellspacing='0' id='all'>
							<tbody>
							<tr>
								<th style='width:30px;text-align:center;'> <input type='checkbox' name='box_all' id='box_all' onclick="checkAll(1,this.name,this.checked)"/> </th>
								<th style='width:30px'> Id </th>
								<th style='width:200px'> Nama </th>
								<th style='width:300px'> Waktu Daftar </th>
								<th style='width:100px'> Opsi </th>
							</tr>
						<?php
							$query = mysqli_query($connect,"select * from `user`")OR die( mysqli_error($connect) );
							while($dt = mysqli_fetch_array($query)){
								echo "<tr id='tr_$dt[id]'>
										<td style='width:30px;text-align:center;'> <input type='checkbox' name='$dt[id]' id='$dt[id]'/> </td>
										<td style='width:30px;text-align:center;' onclick='my_id(\"$dt[id]\");'> 
											<span id='id$dt[id]'>$dt[id]</span> 
											<input type='text' id='id_$dt[id]' value='$dt[id]' class='col-md-12 hides' onblur='my_blur(\"$dt[id]\");' onkeypress='my_blur_id(event,\"$dt[id]\")'/>
											<input type='hidden' name='ids$dt[id]' id='ids$dt[id]' value='$dt[id]' />
										</td>
										<td style='width:200px' onclick='nama(\"$dt[id]\");'> 
											<span id='nama$dt[id]'>$dt[nama]</span> 
											<input type='text' id='nama_$dt[id]' value='$dt[nama]' class='col-md-12 hides' onblur='my_blur(\"$dt[id]\");' onkeypress='my_blur_name(event,\"$dt[id]\")'/>
										</td>
										<td style='width:300px' onclick='date(\"$dt[id]\");'> 
											<span id='date$dt[id]'>$dt[createdat]</span> 
											<input type='text' id='date_$dt[id]' value='$dt[createdat]' class='col-md-12 hides' onblur='my_blur(\"$dt[id]\");' onkeypress='my_blur_date(event,\"$dt[id]\")'/>
										</td>
										<td style='width:100px'>
											<a onclick='edit($dt[id]);'><span class='glyphicon glyphicon-pencil'></span></a>
											<a onclick='del($dt[id]);'><span class='glyphicon glyphicon-minus-sign'></span></a>
										</td>
									</tr>";
							}
						?>
							
							<tr>
								<td colspan='5'>
									<ul class='change'>
										<li><a onclick='del_all();'><span class='glyphicon glyphicon-remove'></span>Hapus semua</a></li>
										<li><a onclick='insert();'><span class='glyphicon glyphicon-plus-sign'></span>Tambah data</a></li>
									</ul>
								</td>
							</tr>
							</tbody>
						</table>
					</form>
				</div>
			</center>
		</div>
	</div>
<script type='text/javascript'>
	function checkAll(no,nama,check){
		for(var i=0;i<document.forms[no].elements.length;i++){
			var e=document.forms[no].elements[i];
			if ( (e.name!=nama) && (e.type == 'checkbox') ){
				e.checked = check;
			}
		}
	}
	function log_out(){
		$.ajax({
			type:'POST',
			data:'nav_log_out=out',
			success:function(msg){
				window.location='<?php echo str_replace('tabel.php','login.php',$_SERVER['PHP_SELF']); ?>';
			}
		});
	}
	function my_blur(i){
		$('#id'+i).show();
		$('#id_'+i).hide();
		$('#nama'+i).show();
		$('#nama_'+i).hide();
		$('#date'+i).show();
		$('#date_'+i).hide();
	}
	function my_blur_id(event,i){
		if(event.keyCode == 13){
			my_blur(i);
			$.ajax({
				type:'POST',
				url:'',
				data:'nav_id='+$('#id_'+i).val()+'&id='+$('#ids'+i).val(),
				dataType:'json',
				success:function(msg){
					$('#id'+i).html(msg.id);
					//alert(msg.ket);
				}
			});
		}
	}
	function my_blur_name(event,i){
		if(event.keyCode == 13){
			my_blur(i);
			$.ajax({
				type:'POST',
				url:'',
				data:'nav_nama='+$('#nama_'+i).val()+'&id='+$('#ids'+i).val(),
				dataType:'json',
				success:function(msg){
					$('#nama'+i).html(msg.nama);
					//alert(msg.ket);
				}
			});
		}
	}
	function my_blur_date(event,i){
		if(event.keyCode == 13){
			my_blur(i);
			$.ajax({
				type:'POST',
				url:'',
				data:'nav_date='+$('#date_'+i).val()+'&id='+$('#ids'+i).val(),
				dataType:'json',
				success:function(msg){
					$('#date'+i).html(msg.date);
					//alert(msg.ket);
				}
			});
		}
	}
	function my_id(i){
		$('#nama'+i).show();
		$('#nama_'+i).hide();
		$('#date'+i).show();
		$('#date_'+i).hide();
		$('#id'+i).hide();
		$('#id_'+i).show().focus();
		
	}
	function nama(i){
		$('#id'+i).show();
		$('#id_'+i).hide();
		$('#date'+i).show();
		$('#date_'+i).hide();
		$('#nama'+i).hide();
		$('#nama_'+i).show().focus();
	}
	function date(i){
		$('#id'+i).show();
		$('#id_'+i).hide();
		$('#nama'+i).show();
		$('#nama_'+i).hide();
		$('#date'+i).hide();
		$('#date_'+i).show().focus();
	}
	function edit(i){
		$('#id').val($('#id_'+i).val());
		$('#nama').val($('#nama_'+i).val());
		$('#date').val($('#date_'+i).val());
		$('#ket').val('Edit data');
		$('#i').val(i);
		$('#full').show();
		return false;
	}
	function del(i){
		if(confirm('Apa anda yakin untuk menghapus data dengan id '+i+' ?')==true){
			$.ajax({
				type:'POST',
				url:'',
				data:'nav_del=nav_del&id='+$('#id_'+i).val(),
				dataType:'json',
				success:function(msg){
					$('#all tbody #tr_'+(i)).remove();
					//alert(msg.ket);
				}
			});
		}
		return false;
	}
	function insert(){
		$('#id').attr({'disabled':'disabled'});
		$('#date').attr({'disabled':'disabled'});
		$('#ket').val('Insert data');
		$('#full').show();
		$('#nama').focus();
		return false;
	}
	function del_all(){ //OK
		var b = '';
		for(var i=0;i<document.forms[1].elements.length;i++){
			var e=document.forms[1].elements[i];
			if( (e.checked == true) && (e.id != 'box_all') ){
				b = b+document.getElementById('ids'+e.id).value+',';
			}
		}
		b = b.substr(0,(b.length-1));
		if(b != ''){
			if(confirm('Apa anda yakin untuk menghapus data dengan id '+b+' ?')==true){
				$.ajax({
					type:'POST',
					url:'',
					data:'nav_del_all=del_all&id='+b,
					dataType:'json',
					success:function(msg){
						var size = $('#all tbody').find('tr').size();
						for(var i = 0; i<msg.jml; i++){
							$('#all tbody #tr_'+(msg[i])).remove();
						}
						//alert(msg.ket);
					}
				});
			}
		}else{ alert('Please select checkboxs!!'); }
		return false;
	}
	/*function del_all(){
		if(confirm('Apa anda yakin untuk menghapus seluruh data yang ada?')==true){
			$.ajax({
				type:'POST',
				url:'',
				data:'nav_del_all=del_all',
				dataType:'json',
				success:function(msg){
					var size = $('#all tbody').find('tr').size();
					for(var i = 1; i<=(msg.jml-1); i++){
						$('#all tbody #tr_'+(msg.i)).remove();
					}
					//alert(msg.ket);
				}
			});
		}
		return false;
	}*/
	function close_full(){
		$('#full').hide();
		return false;
	}
	function db(){
		$('#full').hide();
		var i = $('#i').val();
		$.ajax({
			type:'POST',
			url:'',
			data:'id='+$('#id').val()+'&nama='+$('#nama').val()+'&date='+$('#date').val()+'&ket='+$('#ket').val()+'&i='+i,
			dataType:'json',
			success:function(msg){
				//alert(msg.ket);
				if(msg.jenis == 'edit'){
					$('#id_'+i).val(msg.id);
					$('#id'+i).html(msg.id);
					$('#nama_'+i).val(msg.nama);
					$('#nama'+i).html(msg.nama);
					$('#date_'+i).val(msg.date);
					$('#date'+i).html(msg.date);
				}else{
					$('#all tbody tr:last-child').before(""+
	"<tr id='tr_"+(i+1)+"'>"+
		"<td style='width:30px;text-align:center;'> <input type='checkbox' name='box"+(i+1)+"' id='box"+(i+1)+"'/> </td>"+
		"<td style='width:30px;text-align:center;' onclick='my_id(\""+(i+1)+"\");'>"+
			"<span id='id"+(i+1)+"'>"+msg.id+"</span> "+
			"<input type='text' id='id_"+(i+1)+"' value='"+msg.id+"' class='col-md-12 hides' onblur='my_blur(\""+(i+1)+"\");' onkeypress='my_blur_id(event,\""+(i+1)+"\")'/>"+
			"<input type='hidden' name='ids"+(i+1)+"' id='ids"+(i+1)+"' value='"+msg.id+"' />"+
		"</td>"+
		"<td style='width:200px' onclick='nama(\""+(i+1)+"\");'> "+
			"<span id='nama"+(i+1)+"'>"+msg.nama+"</span> "+
			"<input type='text' id='nama_"+(i+1)+"' value='"+msg.nama+"' class='col-md-12 hides' onblur='my_blur(\""+(i+1)+"\");' onkeypress='my_blur_name(event,\""+(i+1)+"\")'/>"+
		"</td>"+
		"<td style='width:300px' onclick='date(\""+(i+1)+"\");'> "+
			"<span id='date"+(i+1)+"'>"+msg.date+"</span> "+
			"<input type='text' id='date_"+(i+1)+"' value='"+msg.date+"' class='col-md-12 hides' onblur='my_blur(\""+(i+1)+"\");' onkeypress='my_blur_date(event,\""+(i+1)+"\")'/>"+
		"</td>"+
		"<td style='width:100px'>"+
			"<a onclick='edit("+(i+1)+");'><span class='glyphicon glyphicon-pencil'></span></a>"+
			"<a onclick='del("+(i+1)+");'><span class='glyphicon glyphicon-minus-sign'></span></a>"+
		"</td>"+
	"</tr>");
				}
			}
		});
		return false;
	}
</script>
</body>
</html>
